FROM docker.io/library/python:3.12.7-slim-bookworm as build

COPY . /app
WORKDIR /app

RUN apt-get -y update &&\
    pip install --upgrade pip &&\
    python -m pip install --upgrade build &&\
    python -m build --outdir /app/out

FROM docker.io/library/python:3.12.7-slim-bookworm

ENV APP_VER=0.1.0

WORKDIR /app
COPY --from=build /app/out/ns_conf-${APP_VER}-py3-none-any.whl ./
RUN apt-get -y update &&\
    python -m pip install ns_conf-${APP_VER}-py3-none-any.whl
