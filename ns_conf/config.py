import re
import yaml

from loguru import logger
from ns_conf.kubernetes import KubernetesClient


class ValidationError(Exception):
    pass


FIELD_NAME_PATTERN = re.compile('[a-z0-9A-Z]([a-z0-9A-Z-._]*[a-z0-9A-Z]|)')
NAMESPACE_NAME_PATTERN = re.compile('[a-z0-9]([a-z0-9-]*[a-z0-9]|)')
DNS_PATTERN = re.compile(
    '(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])')


class Config(dict):

    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)


def config_factory(file_name: str, kube_client: KubernetesClient = None) -> Config:
    if file_name is None:
        file_name = '/dev/stdin'
    with open(file_name, 'r') as f:
        data = f.read()

    config = yaml.full_load(data)
    _validate_syntax(config)
    _validate_values_with_cluster(config, kube_client)
    return Config(config)


def _validate_syntax(config: dict[str, any]) -> None:
    if 'namespaces' in config and type(config['namespaces']) is list:
        for ns in config['namespaces']:
            _validate_namespace(ns)
    else:
        raise ValidationError('No namespaces or is not a list.')


def _validate_namespace(ns: dict[str, any]) -> None:
    if 'name' not in ns:
        raise ValidationError('No name in namespace item.')
    if not _is_valid_k8s_namespace_name(ns['name']):
        raise ValidationError(f"Not valid namespace name {ns['name']}.")
    if ('team' in ns) == ('user' in ns):
        raise ValidationError(f'Namespace item must have either team or user, not both nor none: {ns}')
    if 'labels' in ns and not _are_valid_key_value_pairs(ns['labels']):
        raise ValidationError(f'Invalid labels in namespace {ns}')
    if 'annotations' in ns and not _are_valid_key_value_pairs(ns['annotations']):
        raise ValidationError(f'Invalid annotations in namespace {ns}')


def _are_valid_key_value_pairs(items: dict[str, any]) -> bool:
    for k, v in items.items():
        if not _is_valid_k8s_field_name(v):
            return False
        k_split = k.split("/")
        if len(k_split) == 1 and _is_valid_k8s_field_name(k):
            continue
        elif len(k_split) == 2 and _is_valid_dns(k[0]) and _is_valid_k8s_field_name(k[1]):
            continue
        else:
            return False
    return True


def _is_valid_dns(name: str) -> bool:
    if len(name) > 253:
        return False
    match = DNS_PATTERN.match(name)
    if match is None or match.group() != name:
        return False
    return True


def _is_valid_k8s_field_name(name: str) -> bool:
    if name is None:
        return True
    if len(name) > 63:
        return False
    match = FIELD_NAME_PATTERN.match(name)
    if match is None or match.group() != name:
        return False
    return True


def _is_valid_k8s_namespace_name(name: str) -> bool:
    if name is None:
        return True
    if len(name) > 63:
        return False
    match = NAMESPACE_NAME_PATTERN.match(name)
    if match is None or match.group() != name:
        return False
    return True


def _validate_values_with_cluster(config: dict[str, any], kube_client: KubernetesClient) -> None:
    if kube_client is None:
        logger.info("User and team names will not be validated.")
        return
    all_users = list(kube_client.get_users())
    all_groups = list(map(lambda t: t[0], kube_client.get_groups()))
    for ns in config['namespaces']:
        _validate_namespace_with_users_and_groups(ns, all_users, all_groups)


def _validate_namespace_with_users_and_groups(ns: dict[str, any], all_users: list[str], all_groups: list[str]) -> None:
    if 'user' in ns:
        if not ns['user'] in all_users:
            raise ValidationError(f"User {ns['user']} does not exist in cluster.")
    if 'team' in ns:
        if not ns['team'] in all_groups:
            raise ValidationError(f"Group/Team {ns['team']} does not exist in cluster.")
