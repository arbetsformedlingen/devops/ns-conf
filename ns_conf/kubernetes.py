from typing import Iterator

import kubernetes


def _unique_list_of_dicts(data: list[dict[any, any]]) -> list[dict[any, any]]:
    def key_fn(v: dict[any, any]) -> str:
        return v.__str__()
    return list({key_fn(i): i for i in data}.values())


class KubernetesClient:

    def __init__(self,
                 k8s_module=kubernetes):
        self._k8s = k8s_module
        try:
            self._k8s.config.load_incluster_config()
            self._in_cluster = True
        except self._k8s.config.ConfigException:
            try:
                self._k8s.config.load_kube_config()
                self._in_cluster = False
            except self._k8s.config.ConfigException:
                raise Exception("Could not configure kubernetes python client")

    def get_users(self) -> Iterator[str]:
        user_objects = self._k8s.client.CustomObjectsApi() \
            .list_cluster_custom_object('user.openshift.io', 'v1', 'users')['items']
        return map(lambda i: i['metadata']['name'], user_objects)

    def get_groups(self) -> Iterator[any]:
        group_objects = self._k8s.client.CustomObjectsApi() \
            .list_cluster_custom_object('user.openshift.io', 'v1', 'groups')['items']
        return map(lambda i: (i['metadata']['name'], i['users']), group_objects)

    def get_namespaces(self) -> list[dict[str, any]]:
        """
        :return: List of namespaces resources.
        """
        return self._k8s.client.CoreV1Api().list_namespace().items

    def get_namespace(self, namespace: str) -> dict[str, any] | None:
        try:
            return self._k8s.client.CoreV1Api().read_namespace(namespace).to_dict()
        except self._k8s.client.exceptions.ApiException:
            return None

    def create_namespace(self, name: str, labels: dict[str, any] = {}, annotations: dict[str, any] = {}):
        body = {'metadata': {'name': name, 'labels': labels, 'annotations': annotations}}
        self._k8s.client.CoreV1Api().create_namespace(body)

    def add_metadata_to_namespace(self, namespace: str, labels: dict[str, any] = {}, annotations: dict[str, any] = {}):
        patch = {'metadata': {'labels': labels, 'annotations': annotations}}
        self._k8s.client.CoreV1Api().patch_namespace(namespace, patch)

    def get_role_binding(self, namespace: str, role_binding_name: str) -> dict[str, any] | None:
        try:
            return self._k8s.client.RbacAuthorizationV1Api()\
                .read_namespaced_role_binding(role_binding_name, namespace).to_dict()
        except self._k8s.client.exceptions.ApiException:
            return None

    @staticmethod
    def _create_subject_from_users_and_groups(users: list[str] = [], groups: list[str] = []) -> list[any]:
        result = []
        if groups is not None:
            for g in groups:
                result.append({
                    'api_group': 'rbac.authorization.k8s.io',
                    'kind': 'Group',
                    'name': g,
                    'namespace': None
                })
        if users is not None:
            for u in users:
                result.append({
                    'api_group': 'rbac.authorization.k8s.io',
                    'kind': 'User',
                    'name': u,
                    'namespace': None
                })
        return result

    def set_access_in_namespace(self, namespace: str, role_binding_name: str, cluster_role: str,
                                users: list[str], groups: list[str]):
        current_role_binding = self.get_role_binding(namespace, role_binding_name)
        if current_role_binding is None:
            body = {'metadata': {
                      'name': role_binding_name},
                    'roleRef': {
                        'api_group': 'rbac.authorization.k8s.io',
                        'kind': 'ClusterRole',
                        'name': cluster_role
                    },
                    'subjects': self._create_subject_from_users_and_groups(users, groups)
                    }
            self._k8s.client.RbacAuthorizationV1Api().create_namespaced_role_binding(namespace, body)
        else:
            current_subjects = current_role_binding['subjects']
            if current_subjects is None:
                current_subjects = []
            desired_subjects = _unique_list_of_dicts(
                current_subjects + self._create_subject_from_users_and_groups(users, groups))
            if desired_subjects != current_subjects:
                patch = {'subjects': desired_subjects}
                self._k8s.client.RbacAuthorizationV1Api().patch_namespaced_role_binding(
                    role_binding_name, namespace, patch)


def _is_dict_part_of_dict(full_dict: dict[str, str] | None, part_dict: dict[str, str] | None) -> bool:
    if part_dict is None:
        return True
    if full_dict is None:
        return False
    for k in part_dict:
        if k not in full_dict:
            return False
        if part_dict[k] != full_dict[k]:
            return False
    return True


def namespace_has_labels(namespace: dict[str, any], desired_labels:dict[str, str]) -> bool:
    current_labels = namespace['metadata'].get('labels', {})
    return _is_dict_part_of_dict(current_labels, desired_labels)


def namespace_has_annotations(namespace: dict[str, any], desired_annotations: dict[str, str]) -> bool:
    current_annotations = namespace['metadata'].get('annotations', {})
    return _is_dict_part_of_dict(current_annotations, desired_annotations)
