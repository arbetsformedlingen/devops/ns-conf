import kubernetes.config
from loguru import logger


class ConfigException(Exception):
    pass


def load_incluster_config():
    logger.info("Loading kubernetes in cluster config")
    try:
        kubernetes.config.load_incluster_config()
    except kubernetes.config.ConfigException as e:
        raise ConfigException(e)


def load_kube_config():
    logger.info("Loading Kubernetes config.")
    kubernetes.config.load_kube_config()