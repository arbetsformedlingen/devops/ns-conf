from ns_conf.kubernetes import KubernetesClient, namespace_has_labels, namespace_has_annotations


def _desired_labels(team: str, user: str, labels: dict[str, str]) -> dict[str, str]:
    result = labels.copy()
    if team is not None:
        result['jobtechdev.se/team'] = team
    if user is not None:
        result['jobtechdev.se/user'] = user
    return result


def _desired_annotations(user: str, annotations: dict[str, str]) -> dict[str, str]:
    result = annotations.copy()
    if user is not None:
        result['openshift.io/requester'] = user
    return result


def sync_namespace(desired_config: dict[str, any], kube_client: KubernetesClient) -> None:
    desired_labels = _desired_labels(
        desired_config.get('team', None),
        desired_config.get('user', None),
        desired_config.get('labels', {}))
    desired_annotations = _desired_annotations(
        desired_config.get('user', None),
        desired_config.get('annotations', {}),
    )
    namespace = kube_client.get_namespace(desired_config['name'])
    if namespace is None or namespace == {}:
        kube_client.create_namespace(desired_config['name'], desired_labels, desired_annotations)
    else:
        if not(namespace_has_labels(namespace, desired_labels)
               and namespace_has_annotations(namespace, desired_annotations)):
            kube_client.add_metadata_to_namespace(desired_config['name'], desired_labels, desired_annotations)

    kube_client.set_access_in_namespace(
        desired_config['name'],
        'admin',
        'admin',
        [desired_config['user']] if 'user' in desired_config else [],
        [desired_config['team']] if 'team' in desired_config else [])
