import unittest
from unittest.mock import Mock, MagicMock

import kubernetes.client
from kubernetes.client.models.v1_namespace import V1Namespace

from ns_conf.kubernetes import KubernetesClient
import ns_conf.kubernetes

# Constants to ease test code.
ANNOTATIONS = {
    'annotation1': 'annotation_value_1',
    'annotation2': 'annotation_value_2'
}
LABELS = {
    'label1': 'label_value_1',
    'label2': 'label_value_2'
}
NAMESPACE_NAME = 'my_namespace'
ROLE_BINDING_NAME = 'my_role_binding'


class KubernetesClientTest(unittest.TestCase):

    def setUp(self) -> None:
        """
        Create an attribute k8s_mock corresponding to the kubernetes module. It contains
        the api clients for CoreV1Api, CustomObjectsApi and  exceptions.
        :return:
        """
        self.k8s_mock = Mock()
        self.k8s_mock.client = Mock()
        self.client = KubernetesClient(k8s_module=self.k8s_mock)

        self.core_v1_api_mock = Mock()
        self.k8s_mock.client.CoreV1Api = lambda: self.core_v1_api_mock

        self.rbac_authorization_v1_api = Mock()
        self.k8s_mock.client.RbacAuthorizationV1Api = lambda: self.rbac_authorization_v1_api

        self.custom_objects_api_mock = Mock()
        self.k8s_mock.client.CustomObjectsApi = lambda: self.custom_objects_api_mock

        self.k8s_mock.client.exceptions = Mock()
        self.k8s_mock.client.exceptions.ApiException = Exception

    def test_get_users(self) -> None:
        self.custom_objects_api_mock.list_cluster_custom_object = \
            MagicMock(return_value={'items': [{'metadata': {'name': 'my_user'}}]})

        result = list(self.client.get_users())

        self.assertEqual(result, ['my_user'])
        self.custom_objects_api_mock.list_cluster_custom_object.assert_called_with('user.openshift.io', 'v1', 'users')

    def test_get_groups(self) -> None:
        self.custom_objects_api_mock.list_cluster_custom_object = \
            MagicMock(return_value={'items': [{'metadata': {'name': 'my_group'}, 'users': ['user_a', 'user_b']}]})

        result = list(self.client.get_groups())

        self.assertEqual(result, [('my_group', ['user_a', 'user_b'])])
        self.custom_objects_api_mock.list_cluster_custom_object.assert_called_with('user.openshift.io', 'v1', 'groups')

    def test_get_namespace_when_exists(self) -> None:
        expected_result = {'api_version': None, 'kind': None, 'metadata': None, 'spec': None, 'status': None}
        self.core_v1_api_mock.read_namespace = MagicMock(return_value=V1Namespace())

        result = self.client.get_namespace(NAMESPACE_NAME)

        self.assertEqual(result, expected_result)
        self.core_v1_api_mock.read_namespace.assert_called_with(NAMESPACE_NAME)

    def test_get_namespace_when_not_exists(self) -> None:
        self.core_v1_api_mock.read_namespace = Mock()
        self.core_v1_api_mock.read_namespace.side_effect = self.k8s_mock.client.exceptions.ApiException

        result = self.client.get_namespace(NAMESPACE_NAME)

        self.core_v1_api_mock.read_namespace.assert_called_with(NAMESPACE_NAME)
        self.assertIsNone(result)

    def test_create_namespace_with_labels_and_annotations(self) -> None:
        self.core_v1_api_mock.create_namespace = MagicMock()

        self.client.create_namespace(NAMESPACE_NAME, LABELS, ANNOTATIONS)

        self.core_v1_api_mock.create_namespace.assert_called_with(
            {'metadata': {
                'name': NAMESPACE_NAME,
                'labels': LABELS,
                'annotations': ANNOTATIONS}})

    def test_create_namespace_without_labels_and_annotations(self) -> None:
        self.core_v1_api_mock.create_namespace = MagicMock()

        self.client.create_namespace(NAMESPACE_NAME, LABELS, ANNOTATIONS)

        self.core_v1_api_mock.create_namespace.assert_called_with(
            {'metadata': {
                'name': NAMESPACE_NAME,
                'labels': LABELS,
                'annotations': ANNOTATIONS}})

    def test_add_metadata_to_namespace(self) -> None:
        self.core_v1_api_mock.patch_namespace = MagicMock()

        self.client.add_metadata_to_namespace(NAMESPACE_NAME, LABELS, ANNOTATIONS)

        self.core_v1_api_mock.patch_namespace.assert_called_with(
            NAMESPACE_NAME,
            {'metadata': {
                'labels': LABELS,
                'annotations': ANNOTATIONS
            }}
        )

    def test_get_role_binding_when_exists(self) -> None:
        expected_result = {'metadata': {}}
        v1_role_binding_mock = Mock()
        v1_role_binding_mock.to_dict = lambda: expected_result
        self.rbac_authorization_v1_api.read_namespaced_role_binding = MagicMock(return_value=v1_role_binding_mock)

        result = self.client.get_role_binding(NAMESPACE_NAME, ROLE_BINDING_NAME)

        self.assertEqual(expected_result, result)
        self.rbac_authorization_v1_api.read_namespaced_role_binding.assert_called_with(ROLE_BINDING_NAME,
                                                                                       NAMESPACE_NAME)

    def test_get_role_binding_when_not_exists(self) -> None:
        self.rbac_authorization_v1_api.read_namespaced_role_binding = Mock()
        self.rbac_authorization_v1_api.read_namespaced_role_binding.side_effect = self.k8s_mock.client.exceptions.ApiException

        result = self.client.get_role_binding(NAMESPACE_NAME, ROLE_BINDING_NAME)

        self.rbac_authorization_v1_api.read_namespaced_role_binding.assert_called_with(ROLE_BINDING_NAME,
                                                                                       NAMESPACE_NAME)
        self.assertIsNone(result)

    def test_create_subject_from_users_and_groups_with_only_users(self) -> None:
        self.assertEqual(
            [{'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'user1', 'namespace': None},
             {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'user2', 'namespace': None}],
            KubernetesClient._create_subject_from_users_and_groups(['user1', 'user2']))

    def test_create_subject_from_users_and_groups_with_only_groups(self) -> None:
        self.assertEqual(
            [{'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'group1', 'namespace': None},
             {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'group2', 'namespace': None}],
            KubernetesClient._create_subject_from_users_and_groups(groups=['group1', 'group2']))

    def test_create_subject_from_users_and_groups(self) -> None:
        self.assertEqual(
            [{'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'group1', 'namespace': None},
             {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'group2', 'namespace': None},
             {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'user1', 'namespace': None},
             {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'user2', 'namespace': None}],
            KubernetesClient._create_subject_from_users_and_groups(['user1', 'user2'], ['group1', 'group2']))

    def test_create_subject_from_users_and_groups_with_empty_values(self) -> None:
        self.assertEqual(
            [],
            KubernetesClient._create_subject_from_users_and_groups([], []))

    def test_set_access_in_namespace_new_role_binding(self) -> None:
        def stub_get_role_binding(ns, rb):
            self.assertEqual(NAMESPACE_NAME, ns)
            self.assertEqual(ROLE_BINDING_NAME, rb)
            return None
        self.client.get_role_binding = stub_get_role_binding
        create_namespaced_role_binding = Mock()
        self.rbac_authorization_v1_api.create_namespaced_role_binding = create_namespaced_role_binding

        self.client.set_access_in_namespace(NAMESPACE_NAME, ROLE_BINDING_NAME, 'my_cluster_role', ['user1'], ['group1'])

        create_namespaced_role_binding.assert_called_with(
            NAMESPACE_NAME,
            {'metadata': {'name': 'my_role_binding'},
             'roleRef': {'api_group': 'rbac.authorization.k8s.io', 'kind': 'ClusterRole', 'name': 'my_cluster_role'},
             'subjects': [
                 {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'group1', 'namespace': None},
                 {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'user1', 'namespace': None}]})

    def test_set_access_in_namespace_update_role_binding(self) -> None:
        def stub_get_role_binding(ns, rb):
            self.assertEqual(NAMESPACE_NAME, ns)
            self.assertEqual(ROLE_BINDING_NAME, rb)
            return {
                'metadata': {'name': rb},
                'roleRef': {'api_group': 'rbac.authorization.k8s.io', 'kind': 'ClusterRole', 'name': 'my_cluster_role'},
                'subjects': [
                    {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'existing_group', 'namespace': None},
                    {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'existing_user', 'namespace': None}]}
        self.client.get_role_binding = stub_get_role_binding
        patch_namespaced_role_binding = Mock()
        self.rbac_authorization_v1_api.patch_namespaced_role_binding = patch_namespaced_role_binding

        self.client.set_access_in_namespace(NAMESPACE_NAME, ROLE_BINDING_NAME, 'my_cluster_role', ['user1'], ['group1'])

        patch_namespaced_role_binding.assert_called_with(
            ROLE_BINDING_NAME,
            NAMESPACE_NAME,
            {'subjects': [
                 {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'existing_group', 'namespace': None},
                 {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'existing_user', 'namespace': None},
                 {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'group1', 'namespace': None},
                 {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'user1', 'namespace': None}]})

    def test_set_access_in_namespace_update_role_binding_with_same_values(self) -> None:
        def stub_get_role_binding(ns, rb):
            return {
                'metadata': {'name': rb},
                'roleRef': {'api_group': 'rbac.authorization.k8s.io', 'kind': 'ClusterRole', 'name': 'my_cluster_role'},
                'subjects': [
                    {'api_group': 'rbac.authorization.k8s.io', 'kind': 'Group', 'name': 'existing_group', 'namespace': None},
                    {'api_group': 'rbac.authorization.k8s.io', 'kind': 'User', 'name': 'existing_user', 'namespace': None}]}
        self.client.get_role_binding = stub_get_role_binding
        patch_namespaced_role_binding = Mock()
        self.rbac_authorization_v1_api.patch_namespaced_role_binding = patch_namespaced_role_binding

        self.client.set_access_in_namespace(NAMESPACE_NAME, ROLE_BINDING_NAME, 'my_cluster_role',
                                            ['existing_user'], ['existing_group'])

        patch_namespaced_role_binding.assert_not_called()


class KubernetesSupportFunctionsTest(unittest.TestCase):

    def test_is_dict_part_of_dict_when_both_is_none(self):
        self.assertTrue(ns_conf.kubernetes._is_dict_part_of_dict(None, None))

    def test_is_dict_part_of_dict_when_full_is_none(self):
        self.assertFalse(ns_conf.kubernetes._is_dict_part_of_dict(None, {'foo': 'bar'}))

    def test_is_dict_part_of_dict_when_both_is_empty(self):
        self.assertTrue(ns_conf.kubernetes._is_dict_part_of_dict({}, {}))

    def test_is_dict_part_of_dict_when_full_is_empty(self):
        self.assertFalse(ns_conf.kubernetes._is_dict_part_of_dict({}, {'foo': 'bar'}))

    def test_is_dict_part_of_dict_when_part_is_empty(self):
        self.assertTrue(ns_conf.kubernetes._is_dict_part_of_dict({'foo': 'bar'}, {}))

    def test_is_dict_part_of_dict_when_full_contains_part(self):
        self.assertTrue(ns_conf.kubernetes._is_dict_part_of_dict(
            {'foo': 'bar', 'gazonk': 'foobar'}, {'gazonk': 'foobar'}))

    def test_is_dict_part_of_dict_when_value_differ(self):
        self.assertFalse(ns_conf.kubernetes._is_dict_part_of_dict(
            {'foo': 'bar', 'gazonk': 'foobar'}, {'gazonk': 'barfoo'}))


if __name__ == '__main__':
    unittest.main()
